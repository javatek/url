package org.bitbucket.javatek.url;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URLConnection;

import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;
import static org.bitbucket.javatek.require.StringRequires.requireNonEmpty;

/**
 *
 */
public final class URL {
  private final java.net.URL url;

  public URL(java.net.URL url) {
    this.url = requireNonNull(url);
  }

  public URL(URI uri) throws MalformedURLException {
    this(uri.toURL());
  }

  public URL(@Nonnull String spec) {
    this(parseUrl(spec));
  }

  private static java.net.URL parseUrl(String spec) {
    try {
      return new java.net.URL(spec);
    }
    catch (MalformedURLException e) {
      throw new IllegalArgumentException(e);
    }
  }

  private URL clone(@Nullable String path, @Nullable String query) {
    query = query != null ? query : "";
    path = path != null ? "/" + stripStartSlash(path) : "";
    String file = query.isEmpty() ? path : path + "?" + query;
    try {
      return new URL(
        new java.net.URL(url, file)
      );
    }
    catch (MalformedURLException e) {
      throw new IllegalArgumentException(e);
    }
  }

  /**
   * Extends the url with new portion of path.
   * <pre>
   *   new URL("http://host").withPath("path") = "http://host/path"
   *   new URL("http://host/path1").withPath("path2") = "http://host/path1/path2"
   *   new URL("http://host/path1?key=value").withPath("/path2") = "http://host/path1/path2?key=value"
   * </pre>
   * @param path new portion of path to add at the end of path
   * @return url with new path
   */
  public URL withPath(String path) {
    requireNonEmpty(path);
    path = joinPath(url.getPath(), path);
    return clone(path, url.getQuery());
  }

  private String joinPath(String parent, String child) {
    return stripEndSlash(parent) + "/" + stripStartSlash(child);
  }

  private String stripStartSlash(String path) {
    return path.startsWith("/")
      ? path.substring(1)
      : path;
  }

  private String stripEndSlash(String path) {
    return path.endsWith("/")
      ? path.substring(0, path.length() - 1)
      : path;
  }

  /**
   * Extends the url with new query param
   * @param key   param name
   * @param value param value
   * @return url with new param
   */
  public URL withParam(String key, String value) {
    requireNonEmpty(key);
    requireNonNull(value);

    String query =
      url.getQuery() != null
        ? url.getQuery() + "&" + key + "=" + value
        : key + "=" + value;

    return clone(url.getPath(), query);
  }

  //////////////////////////////////////////////////////////////////////////////

  @Nonnull
  public URLParams params() {
    return new URLParams(url.getQuery());
  }

  @Nonnull
  public URLParam param(@Nonnull String name) {
    requireNonEmpty(name);
    return
      new URLParams(url.getQuery())
        .get(name);
  }

  //////////////////////////////////////////////////////////////////////////////

  public URLConnection openConnection() throws IOException {
    return url.openConnection();
  }

  @Override
  public int hashCode() {
    return url.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return o == this ||
      (o != null
        && o.getClass() == URL.class
        && ((URL) o).url.equals(this.url)
      );
  }

  @Override
  public String toString() {
    return url.toString();
  }
}
