package org.bitbucket.javatek.url;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;

import static java.util.Collections.emptyList;
import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;
import static org.bitbucket.javatek.require.StringRequires.requireNonEmpty;

/**
 *
 */
public final class URLParam implements Iterable<String> {
  private final String name;
  private final List<String> values;

  URLParam(@Nonnull String name, @Nonnull List<String> values) {
    this.name = requireNonEmpty(name);
    this.values = requireNonNull(values);
  }

  URLParam(@Nonnull String name) {
    this(name, emptyList());
  }

  URLParam withValue(@Nonnull String value) {
    requireNonNull(value);
    List<String> values =
      new ArrayList<>(this.values);
    values.add(value);
    return new URLParam(name, values);
  }

  public String name() {
    return name;
  }

  @Nonnull
  @Override
  public Iterator<String> iterator() {
    return values.iterator();
  }

  @Nonnull
  public Iterable<String> values() {
    return values;
  }

  @Nullable
  public String firstValue() {
    return
      !values.isEmpty()
        ? values.iterator().next()
        : null;
  }

  @Override
  public String toString() {
    StringJoiner joiner = new StringJoiner("&");
    for (String value : values) {
      joiner.add(name + "=" + value);
    }
    return joiner.toString();
  }

  @Nonnull
  public String toUrlEncodedString() {
    try {
      String name = URLEncoder.encode(this.name, "UTF-8");
      StringJoiner joiner = new StringJoiner("&");
      for (String value : values) {
        joiner.add(name + "=" + URLEncoder.encode(value, "UTF-8"));
      }
      return joiner.toString();
    }
    catch (UnsupportedEncodingException e) {
      throw new AssertionError("UTF-8 not found", e);
    }
  }
}
