package org.bitbucket.javatek.url;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.StringTokenizer;

import static java.util.Collections.singletonList;
import static org.bitbucket.javatek.require.StringRequires.requireNonEmpty;

/**
 *
 */
public final class URLParams implements Iterable<URLParam> {
  private final Map<String, URLParam> params;

  URLParams(@Nullable String query) {
    query = query != null ? query : "";

    Map<String, URLParam> params = new LinkedHashMap<>();
    StringTokenizer tokenizer = new StringTokenizer(query, "&");
    while (tokenizer.hasMoreTokens()) {
      String[] pair = tokenizer.nextToken().split("=");
      try {
        String name = URLDecoder.decode(pair[0], "UTF-8");
        String value =
          pair.length >= 2
            ? URLDecoder.decode(pair[1], "UTF-8")
            : "";

        params.compute(
          name,
          (n, param) -> {
            if (param == null) {
              return new URLParam(name, singletonList(value));
            }
            return param.withValue(value);
          });
      }
      catch (UnsupportedEncodingException e) {
        throw new AssertionError("UTF-8 not found", e);
      }
    }

    this.params = params;
  }

  @Nonnull
  @Override
  public Iterator<URLParam> iterator() {
    return params.values().iterator();
  }

  @Nonnull
  public URLParam get(@Nonnull String name) {
    return
      params.computeIfAbsent(
        requireNonEmpty(name),
        (n) -> new URLParam(name)
      );
  }

  @Override
  public String toString() {
    StringJoiner joiner = new StringJoiner("&");
    for (URLParam param : this) {
      joiner.add(param.toString());
    }
    return joiner.toString();
  }

  @Nonnull
  public String toUrlEncodedString() {
    StringJoiner joiner = new StringJoiner("&");
    for (URLParam param : this) {
      joiner.add(param.toUrlEncodedString());
    }
    return joiner.toString();
  }
}
