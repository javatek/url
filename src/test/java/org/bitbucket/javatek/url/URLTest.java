package org.bitbucket.javatek.url;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

@SuppressWarnings("HttpUrlsUsage")
public final class URLTest {
  @Test
  public void constructFromString() {
    assertEquals("http://yandex.ru", new URL("http://yandex.ru").toString());
    assertEquals("https://yandex.ru", new URL("https://yandex.ru").toString());

    assertEquals(
      "http://yandex.ru/search",
      new URL("http://yandex.ru/search").toString()
    );

    assertEquals(
      "http://yandex.ru/search/?text=wtf&lr=213",
      new URL("http://yandex.ru/search/?text=wtf&lr=213").toString()
    );
  }

  @Test
  public void constructWithPath() {
    assertEquals(
      "http://yandex.ru/search",
      new URL("http://yandex.ru/")
        .withPath("search")
        .toString()
    );

    assertEquals(
      "http://yandex.ru/a/b",
      new URL("http://yandex.ru/a/")
        .withPath("b")
        .toString()
    );

    assertEquals(
      "http://yandex.ru/a/b/c",
      new URL("http://yandex.ru/a/b/")
        .withPath("/c")
        .toString()
    );
  }

  @Test
  public void constructWithParam() {
    assertEquals(
      "http://yandex.ru/search/?text=book&text=book2",
      new URL("http://yandex.ru/search/")
        .withParam("text", "book")
        .withParam("text", "book2")
        .toString()
    );
  }

  @Test
  public void constructWithPathAndParam() {
    assertEquals(
      "https://www.google.com/search?q=docker+gradle&oq=docker+gra",
      new URL("https://www.google.com/")
        .withPath("/search")
        .withParam("q", "docker+gradle")
        .withParam("oq", "docker+gra")
        .toString()
    );
  }

  @Test
  public void getParams1() {
    URL url =
      new URL("https://www.google.com/")
        .withPath("/search")
        .withParam("q", "docker+gradle")
        .withParam("oq", "docker+gra")
        .withParam("p3", "val1")
        .withParam("p3", "val2")
      ;

    assertEquals(
      "q=docker+gradle&oq=docker+gra&p3=val1&p3=val2",
      url.params().toUrlEncodedString()
    );

    assertEquals(
      "q=docker gradle&oq=docker gra&p3=val1&p3=val2",
      url.params().toString()
    );
  }

  @Test
  public void getParams2() {
    URL url =
      new URL("https://www.google.com/")
        .withPath("/search")
        .withParam("p3", "Сим Сим")
      ;

    assertEquals(
      "p3=%D0%A1%D0%B8%D0%BC+%D0%A1%D0%B8%D0%BC",
      url.params().toUrlEncodedString()
    );

    assertEquals(
      "p3=Сим Сим",
      url.params().toString()
    );
  }
}